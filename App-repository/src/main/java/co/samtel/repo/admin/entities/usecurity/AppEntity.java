package co.samtel.repo.admin.entities.usecurity;

import java.math.BigInteger;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import co.samtel.repo.admint.audit.Auditoria;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name ="tbl_app_admin")
@Getter @Setter
public class AppEntity extends Auditoria<String> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rolerest_generator")
	//El SequenceName hace referencia a una tabla en la base de datos el cual contiene un id que se toma como base para autoincrementar.
	@SequenceGenerator(name = "rolerest_generator", sequenceName = "rol_secuence", allocationSize = 1)
	@Column(name="id_app")
	private Long id;
	@Column(name="codigo")
	private String codigo;	
	@Column(name="nombre")
	private String nombre;
	@Column(name ="numero_consumos")
	private BigInteger numeroConsumo;
	@OneToMany(mappedBy = "appAdmin", cascade = CascadeType.ALL)
	private Set<UsuarioEntity> usuarios;
	
	public AppEntity() {
	}	
}